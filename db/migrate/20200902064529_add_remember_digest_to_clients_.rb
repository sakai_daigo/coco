class AddRememberDigestToClients < ActiveRecord::Migration[5.2]
  def change
    add_column :clients, :remember_digest, :string
  end
end
