require 'rails_helper'

RSpec.feature "ClientPages", type: :feature do

  let(:client){ create(:client) }

  feature "signup" do
    scenario "check client_signup_error_messages" do
      visit client_signup_path
      click_button "新規登録"
      expect(page).to have_css "#error_explanation"
    end

    scenario "check no exist client_signup_error_messages" do
      visit client_signup_path
      fill_in "名前", with: "tanaka"
      fill_in "メールアドレス", with: "example@icloud.com"
      fill_in "パスワード", with: "12345678"
      fill_in "パスワード確認", with: "12345678"
      click_button "新規登録"
      expect(page).not_to have_css "#error_explanation"
    end
  end

  feature "edit" do
    scenario "unsuccessful edit" do
      visit client_edit_path(client)
      fill_in "メールアドレス", with: ""
      fill_in "名前", with: ""
      click_button "設定を更新する"
      expect(current_path).to eq "/client/#{client.id}"
      expect(page).to have_content "The form contains 5 errors."
    end
  end

end
