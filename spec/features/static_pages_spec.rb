require 'rails_helper'

RSpec.feature "StaticPages",type: :feature do

  scenario "home title test" do
    visit root_path
    expect(page).to have_title "トップページ||Coco"
  end

  scenario "about title test" do
    visit about_path
    expect(page).to have_title "サービス詳細||Coco"
  end

  scenario "help title test" do
    visit help_path
    expect(page).to have_title "ヘルプ||Coco"
  end

  scenario "contact title test" do
    visit contact_path
    expect(page).to have_title "お問い合わせ||Coco"
  end

  # scenarion "navigation test" do
  #   visit root_path
  #   expect(page).to have_link "カウンセラーを探す"
  #   expect(page).to have_link "Cocoとは"
  #   expect(page).to have_link "新規登録"
  #   expect(page).to have_link "人気順で探す"
  #   expect(page).to have_link "経歴順で探す"
  #   expect(page).to have_link "相談内容から探す"
  #   expect(page).to have_link "良くある質問"
  #   expect(page).to have_link ""
  #   expect(page).to have_link ""
  # end

end
