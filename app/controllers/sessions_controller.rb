class SessionsController < ApplicationController
  def new
  end

  def create
    client = Client.find_by(email: params[:session][:email].downcase)
    if client && client.authenticate(params[:session][:password])
      if client.activated?
        log_in client
        params[:session][:remember_me] == '1' ? remember(client) : forget(client)
        redirect_back_or client
      else
        message = "アカウントが有効化されていません。"
        message += "メールをチェックしてアカウント有効化リンクをクリックしてください"
        flash[:warning] = message
        redirect_to root_url
      end
    else
      flash.now[:danger] = "ログインできませんでした"
      render "new"
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_path
  end

end
