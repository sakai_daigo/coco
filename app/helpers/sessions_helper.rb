module SessionsHelper
  def log_in(client)
    session[:client_id] = client.id
  end

  def current_client
    if (client_id = session[:client_id])
      @current_user ||= Client.find_by(id: client_id)
    elsif (client_id = cookies.signed[:client_id])
      client = Client.find_by(id: client_id)
      if client && client.authenticated?(:remember, cookies[:remember_token])
        log_in client
        @current_client = client
      end
    end
  end

  def current_client?(client)
    client == current_client
  end

  def logged_in?
    !current_client.nil?
  end

  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end

  def forget(client)
    client.forget
    cookies.delete(:client_id)
    cookies.delete(:remember_token)
  end

  def log_out
    forget(current_client)
    session.delete(:client_id)
    @current_client = nil
  end

  def remember(client)
    client.remember
    # binding.pry
    cookies.permanent.signed[:client_id] = client.id
    cookies.permanent[:remember_token] = client.remember_token
  end

end
