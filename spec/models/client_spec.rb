require 'rails_helper'

RSpec.describe Client, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"

  let(:client){ create :client }

  describe "validation" do
    it "correct name and email pattern" do
      expect(client.valid?).to eq(true)
    end

    it "empty name pattern" do
      client.name = ""
      expect(client.valid?).to be_falsey
    end

    it "empty email pattern" do
      client.email = ""
      expect(client.valid?).to be_falsey
    end

    it "too long name pattern" do
      client.name = "a" * 51
      expect(client.valid?).to be_falsey
    end

    it "too long email pattern" do
      client.email = "a" * 249 + "@example.com"
      expect(client.valid?).to be_falsey
    end

    it "empty password pattern" do
      client.password = client.password_confirmation = "" * 6
      expect(client.valid?).to be_falsey
    end

    it "too short password pattern" do
      client.password = client.password_confirmation = "a" * 5
      expect(client.valid?).to be_falsey
    end

    it "wrong email format pattern" do
      client.email = "aaa"
      expect(client.valid?).to be_falsey
    end

    it "correct email format pattern" do
      client.email = "example@icloud.com"
      expect(client.valid?).to eq(true)
    end

    it "not uniquenes email pattern" do
      dupplicate_client = client.dup
      dupplicate_client.email = client.email.upcase
      dupplicate_client.save
      expect(dupplicate_client.valid?).to be_falsey
    end

    it "make sure small email when saved to the db" do
      upcase_email = "NEKO@icloud.com"
      client.email = upcase_email
      client.save
      expect(upcase_email.downcase).to eq(client.reload.email)
    end
  end

  describe "login and logout" do
    it "anthenticate should return false when client with nil digest" do
      client.authenticated?(:remember, "")
    end
  end

end
