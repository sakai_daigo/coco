FactoryBot.define do
  factory :client do
    name { "aaa" }
    email { "neko@icloud.com" }
    password { "password" }
    password_digest { Client.digest("password") }
    activated { true }
    activated_at { Time.zone.now }
    admin { true }
  end

  factory :other_client, class: Client do
    name { "bbb" }
    email { "bbb@icloud.com" }
    password { "password" }
    password_digest { Client.digest("password") }
  end
end
