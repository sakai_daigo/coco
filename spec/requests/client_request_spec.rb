require 'rails_helper'

RSpec.describe "Clients", type: :request do

  let(:client){ create(:client) }

  let(:other_client){ create(:other_client) }

  describe "signup" do
    it "returns http success" do
      get "/client/new"
      expect(response).to have_http_status(:success)
    end

    it "invalid signup information" do
      before_count = Client.count
      post client_signup_path, params: { client: { name: "", email: "example@icloud.com",
                                                    password: "12345678", password_confirmation: "12345678"}}
      expect(before_count).to eq Client.count
    end
  end

  describe "GET /show" do
    it "returns http success" do
      get "/client/show"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /edit" do
    it "returns http success" do
      get "/client/edit"
      expect(response).to have_http_status(:success)
    end

    it "successfull edit" do
      name = "foobar"
      email = "foobar@icloud.com"
      log_in_as(client)
      patch client_update_path(client), params: { client: { name: name,
                                              email: email,
                                              password:              "",
                                              password_confirmation: "" } }
      expect(response).to redirect_to client_path(client)
      expect(flash[:success]).not_to be_empty
      client.reload
      expect(name).to eq client.name
      expect(email).to eq client.email
    end

    it "should redirect edit when not logged in" do
      get client_edit_path(client)
      expect(flash[:danger]).to be_truthy
      expect(response).to redirect_to login_path
    end

    it "should redirect update when not logged in" do
      patch client_update_path(client), params: { user: { name: client.name,
                                              email: client.email } }
      expect(flash[:danger]).to be_truthy
      expect(response).to redirect_to login_path
    end

    it "should redirect edit when logged in as wrong user" do
      log_in_as(other_client)
      get client_edit_path(client)
      expect(flash).to be_empty
      expect(response).to redirect_to root_url
    end

    it "should redirect update when logged in as wrong user" do
      log_in_as(other_client)
      patch client_update_path(client), params: { user: { name: client.name,
                                                email: client.email } }
      expect(flash).to be_empty
      expect(response).to redirect_to root_url
    end

    it "successful edit with friendly forwarding" do
      get client_edit_path(client)
      log_in_as(client)
      expect(response).to redirect_to client_edit_path(client)
    end

  end

  describe "destroy" do
    it "should redirect destroy when not logged in" do
      client
      other_client
      client_count = Client.count
      delete clients_destroy_path(client)
      expect(Client.count).to eq client_count
      expect(response).to redirect_to login_url
    end

    it "should redirect destroy when logged in as a non-admin" do
      client
      other_client
      client_count = Client.count
      log_in_as(other_client)
      delete clients_destroy_path(client)
      expect(Client.count).to eq client_count
      expect(response).to redirect_to root_url
    end

    it "success destroy" do
      client_count = Client.count
      log_in_as(client)
      expect{ delete clients_destroy_path(client) }.to change{ Client.count }.by(-1)
      expect(response).to redirect_to root_url
    end

  end

end
