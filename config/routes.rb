Rails.application.routes.draw do
  # if Rails.env.development?
  #   mount LetterOpenerWeb::Engine, at: '/letter_opener'
  # end
  root "static_pages#home"
  get "/about", to: "static_pages#about", as: "about"
  get "/help", to: "static_pages#help", as: "help"
  get "/contact", to: "static_pages#contact", as: "contact"
  get "client/signup", to: "clients#new"
  post "client/signup", to: "clients#create", as: "clients"
  get "client/:id", to: "clients#show", as: "client"
  get "client/:id/edit", to: "clients#edit", as: "client_edit"
  patch "client/:id", to: "clients#update", as: "client_update"
  delete "client/:id", to: "clients#destroy", as: "clients_destroy"
  get "/login", to: "sessions#new"
  post "/login", to: "sessions#create"
  delete "logout", to: "sessions#destroy"
  resources :account_activations, only: [:edit]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
