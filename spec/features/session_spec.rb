require 'rails_helper'

RSpec.feature "Sessions", type: :feature do
  pending "add some scenarios (or delete) #{__FILE__}"

  feature "login test" do

    let(:client){ create(:client) }

    scenario "check flash when invalid information" do
      visit login_path
      click_button "ログイン"
      expect(page).to have_content "ログインできませんでした"
      visit root_path
      expect(page).not_to have_content "ログインできませんでした"
    end

    scenario "check header change when login" do
      client
      visit login_path
      fill_in "メールアドレス", with: "neko@icloud.com"
      fill_in "パスワード", with: "password"
      click_button "ログイン"
      expect(page).to have_link "ログアウト"
      expect(page).to have_link "Myアカウント"
      click_link "ログアウト"
      expect(page).to have_content "ログイン"
      expect(page).to have_content "新規登録"
      expect(page).to have_content "Cocoとは"
      page.driver.submit :delete, "/logout", {}
      expect(page).to have_content "ログイン"
      expect(page).not_to have_link "ログアウト"
    end
  end
end
