require 'rails_helper'

module SessionsHelper

  def is_logged_in?
    !session[:client_id].nil?
  end

  def log_in_as(client, password: 'password', remember_me: '1')
   post login_path, params: { session: { email: client.email,
                                         password: password,
                                         remember_me: remember_me } }
 end

  # テストユーザーとしてログインする
  # def log_in_as(client)
  #   session[:client_id] = client.id
  # end

  # def log_in_feature(client)
  #   visit login_path
  #   fill_in "メールアドレス", client.email
  #   fill_in "パスワード", "password"
  #   click_button "cl"
  # end
end
