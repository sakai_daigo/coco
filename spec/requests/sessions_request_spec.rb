require 'rails_helper'

RSpec.describe "Sessions", type: :request do

  let(:client){ create(:client) }

  describe "GET /new" do
    it "returns http success" do
      get login_path
      expect(response).to have_http_status(:success)
    end
  end

  describe "login" do
    it "with remember" do
      log_in_as(client)
      expect(cookies[:remember_token]).not_to be_empty
    end

    it "withot remember" do
      log_in_as(client, remember_me: "1")
      delete logout_path
      log_in_as(client, remember_me: "0")
      expect(cookies[:remember_token]).to be_empty
    end

    it "current_user returns right user when session is nil" do
      log_in_as(client)
      expect(client).to eq current_client
    end

    it "current_user returns right user when session is nil" do
      log_in_as(client, remember_me: "1")
      expect(client).to eq current_client
      is_logged_in?
    end

    it "current_user returns nil when remember digest is wrong" do
      remember(client)
      client.update_attribute(:remember_digest, Client.digest(Client.new_token))
      expect(current_client).to be nil
    end
  end

end
