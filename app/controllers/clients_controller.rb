class ClientsController < ApplicationController
  before_action :logged_in_client, only: [:edit, :update, :destroy]
  before_action :correct_client, only: [:edit, :update, :destroy]

  def new
    @client = Client.new
    @form_url = client_signup_path
  end

  def create
    @client = Client.new(client_params)
    if @client.save
      @client.send_activation_email
      flash[:info] = "メールアドレスを確認してアカウント有効化リンクをクリックしてください"
      redirect_to root_path
    else
      render "new"
    end
  end

  def show
  end

  def edit
    @form_url = client_update_path
  end

  def update
    if @client.update_attributes(client_params)
      flash[:success] = "変更完了しました"
      redirect_to @client
    else
      render "edit"
    end
  end

  def destroy
    Client.find_by(id: params[:id]).destroy
    flash[:success] = "退会が完了しました"
    redirect_to root_path
  end


  private

    def client_params
      params.require(:client).permit(:name, :email, :password,
                                      :password_confirmation, :age,
                                       :special_notes, :gender)
    end

    def logged_in_client
      unless logged_in?
        store_location
        flash[:danger] = "ログインしてください"
        redirect_to login_url
      end
    end

    def correct_client
      @client = Client.find(params[:id])
      redirect_to(root_url) unless current_client?(@client)
    end
end
