require "rails_helper"

RSpec.describe ClientMailer, type: :mailer do
  describe "account_activation" do
    # let(:mail) { ClientMailer.account_activation }
    let(:client) { create(:client) }

    it "renders the headers" do
      client.activation_token = Client.new_token
      mail = ClientMailer.account_activation(client)
      expect(mail.subject).to eq("アカウント有効化のためのメール")
      expect(mail.to).to eq([client.email])
      expect(mail.from).to eq(["from@example.com"])
      expect(mail.body.encoded).to match(client.name)
      expect(mail.body.encoded).to match(client.activation_token)
      expect(mail.body.encoded).to match(CGI.escape(client.email))
    end
  end

  describe "password_reset" do
    let(:mail) { ClientMailer.password_reset }

    it "renders the headers" do
      expect(mail.subject).to eq("Password reset")
      expect(mail.to).to eq(["to@example.org"])
      expect(mail.from).to eq(["from@example.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Hi")
    end
  end

end
