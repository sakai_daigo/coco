class AddAgeSpecialNotesGenderToClients < ActiveRecord::Migration[5.2]
  def change
    add_column :clients, :age, :integer
    add_column :clients, :special_notes, :text
    add_column :clients, :gender, :string
  end
end
