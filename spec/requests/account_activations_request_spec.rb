require 'rails_helper'

RSpec.describe "AccountActivations", type: :request do

  it "valid signup information with account activation" do
    get client_signup_path
    expect{ post clients_path, params: { client: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } } }.to change{ Client.count }.by(1)
    expect(ActionMailer::Base.deliveries.size).to eq 1
     client = assigns(:client)
     expect(client.activated?).to be_falsey
     # 有効化していない状態でログインしてみる
     log_in_as(client)
     expect(is_logged_in?).to be_falsey
     # 有効化トークンが不正な場合
     get edit_account_activation_path("invalid token", email: client.email)
     expect(is_logged_in?).to be_falsey
     # トークンは正しいがメールアドレスが無効な場合
     get edit_account_activation_path(client.activation_token, email: 'wrong')
     expect(is_logged_in?).to be_falsey
     # 有効化トークンが正しい場合
     get edit_account_activation_path(client.activation_token, email: client.email)
     expect(client.reload.activated?).to be_truthy
     expect(is_logged_in?).to be_truthy
  end

end
